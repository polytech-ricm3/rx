/******************************************************************************/
/*			Application: Jeu du pendu     */
/******************************************************************************/
/*									      */
/*			 programme  CLIENT  		  */
/*									      */
/******************************************************************************/
/*									      */
/*		Auteurs :  CHATON Alexandra
PASDELOUP Romain
SALMON Alexandre
*/
/*		Date :  17/04/2019				  */
/*									      */
/******************************************************************************/

#include <stdio.h>
//#include <curses.h> /* Primitives de gestion d'ecran */
#include <sys/signal.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "fon.h"		/* primitives de la boite a outils */

#define SERVICE_DEFAUT "1111"
#define SERVEUR_DEFAUT "127.0.0.1"
#define CLEAR_STDIN { int c; while((c = getchar()) != '\n' && c != EOF); }

void client_appli(char *serveur, char *service);

int lettreValide(char suggestion, char *alphabet)
{				// return 0 si n'est pas valide autre chose sinon
	if (!isalpha(suggestion))
		return 0;
	if (suggestion >= 'A' && suggestion <= 'Z')
		suggestion += 32;

	if (alphabet[suggestion - 'a']) {
		return 0;
	} else {
		alphabet[suggestion - 'a'] = 1;
		return 1;
	}
}

/*****************************************************************************/
/*--------------- programme client -----------------------*/

int main(int argc, char *argv[])
{

	char *serveur = SERVEUR_DEFAUT;	/* serveur par defaut */
	char *service = SERVICE_DEFAUT;	/* numero de service par defaut (no de port) */

	/* Permet de passer un nombre de parametre variable a l'executable */
	switch (argc) {
	case 1:		/* arguments par defaut */
		printf("serveur par defaut: %s\n", serveur);
		printf("service par defaut: %s\n", service);
		break;
	case 2:		/* serveur renseigne  */
		serveur = argv[1];
		printf("service par defaut: %s\n", service);
		break;
	case 3:		/* serveur, service renseignes */
		serveur = argv[1];
		service = argv[2];
		break;
	default:
		printf
		    ("Usage:client serveur(nom ou @IP)  service (nom ou port) \n");
		exit(1);
	}

	/* serveur est le nom (ou l'adresse IP) auquel le client va acceder */
	/* service le numero de port sur le serveur correspondant au  */
	/* service desire par le client */

	client_appli(serveur, service);
}

/*****************************************************************************/
void client_appli(char *serveur, char *service)

/* procedure correspondant au traitement du client de votre application */
{
	char *str_recv = malloc(sizeof(char) * 100);
	char nbCoups;
	char suggestion;
	char alphabet[26];	//on a vérifié il a bien 26 lettres
	char *tmp = alphabet;
	char play;

	/* Initialisation de la socket */
	struct sockaddr_in *p_adr_client;
	int numsockclient = h_socket(AF_INET, SOCK_STREAM);	//Création de la socket
	adr_socket(SERVICE_DEFAUT, NULL, SOCK_STREAM, &p_adr_client);	// Configuration de l'adresse de la socket

	int connexion = h_connect(numsockclient, p_adr_client);	//Demande de connexion

	if (connexion == 0)	//Si on a pu se connecter au serveur
	{
		printf(" _           ______ _____ _   _______ _   _ \n");
		printf("| |          | ___ \\  ___| \\ | |  _  \\ | | |\n");
		printf("| |     ___  | |_/ / |__ |  \\| | | | | | | |\n");
		printf("| |    / _ \\ |  __/|  __|| . ` | | | | | | |\n");
		printf("| |___|  __/ | |   | |___| |\\  | |/ /| |_| |\n");
		printf("\\_____/\\___| \\_|   \\____/\\_| \\_/___/  \\___/ \n");
		do {		//Tant que le joueur souhaite jouer
		/** Initialisation du tableau qui mémorise les lettres jouées **/
			tmp = alphabet;
			while (tmp < alphabet + 26)
				*tmp++ = 0;
		/** -------------------------------------------------------- **/

			h_reads(numsockclient, str_recv, 100);	//On reçoit le mot initial de la part du serveur (sous forme de tirets)
			h_reads(numsockclient, &nbCoups, 1);	//On reçoit le nombre de coups restant

			do {	//Tant que la partie n'est pas terminée
				printf("================\n\n");
				printf("%s\n", str_recv);	//Affichage de l'etat du mot à deviner
				printf("Il vous reste %d coups.\n\n", (int)nbCoups);	//Affichage du nombre de coups restant

				do {	//Tant que la saisie de l'utilisateur n'est pas valide
					printf
					    ("Saisissez votre proposition : ");
					suggestion = getc(stdin);	//Récupération d'un caractère saisi au clavier
					printf("\n");
					CLEAR_STDIN;	// Vidage de STDIN pour éviter des bugs
				} while (!lettreValide(suggestion, alphabet));

				h_writes(numsockclient, &suggestion, 100);	//Envoie du caractère au serveur
				h_reads(numsockclient, str_recv, 100);	//Récupération de l'etat du mot
				h_reads(numsockclient, &nbCoups, 1);	//Récupération du nombre de coups restant
			} while (strcmp(str_recv, "jeutermine") != 0
				 && nbCoups != 0);

			// On vérifie de quelle façon on a terminé la partie
			if (nbCoups == 0) {	//Si on a perdu
				printf("Vous avez échoué...\n");
				h_reads(numsockclient, str_recv, 100);
				printf("Le mot à trouver était : %s\n",
				       str_recv);
			} else {	//Si on a gagné
				printf
				    ("Victoire, vous avez trouvé le mot ! Il vous restait %d coups.\n",
				     nbCoups);
			}

			do {	//Tant que l'utilisateur ne rentre pas un caractère attendu
				printf("Souhaitez vous rejouer? (O/N) : ");
				play = getc(stdin);
				printf("\n");
				CLEAR_STDIN;
			} while (play != 'O' && play != 'N' && play != 'o'
				 && play != 'n');

			play = (play == 'O' || play == 'o') ? 1 : 0;	//On récupère la réponse du joueur sous forme booleenne
			h_writes(numsockclient, &play, 1);	//On envoie au serveur la décision du joueur pour savoir s'il doit relancer une partie.
		} while (play);
	}
	h_close(numsockclient);
}

	/*****************************************************************************/
