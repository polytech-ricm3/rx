/******************************************************************************/
/*			Application: Jeu du pendu     */
/******************************************************************************/
/*									      */
/*			 programme  SERVEUR 		  */
/*									      */
/******************************************************************************/
/*									      */
/*		Auteurs :  CHATON Alexandra
PASDELOUP Romain
SALMON Alexandre
*/
/*		Date :  17/04/2019				  */
/*									      */
/******************************************************************************/

#include <stdio.h>
//#include <curses.h>

#include <sys/signal.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <limits.h>

#include "fon.h"		/* Primitives de la boite a outils */

#define SERVICE_DEFAUT "1111"

char motadeviner[100];
char mottrouver[100];
char *filename = "dico.dic";	//Nom du dictionnaire où piocher les mots

void serveur_appli(char *service);	/* programme serveur */

/******************************************************************************/
/*---------------- programme serveur ------------------------------*/

/**
 * check -
 * @c: Caractère à tester
 * Description: Vérifie si le caractère appartient au mot à trouver ou non.
 * 							Et effectue les remplacements s'il y est
 *              Renvoie 0 quand le coup est mauvais et 1 quand il est bon
 */
int check(char c)
{
	int good_guess = 0;
	for (int i = 0; i < strlen(motadeviner); i++) {
		if (c == motadeviner[i] || c + 32 == motadeviner[i]) {	//Gère les majuscules
			mottrouver[i] = motadeviner[i];
			good_guess = 1;
		}
	}
	return good_guess;
}

/**
 * pickRandomWord -
 * Description: Choisi un mot au hasard dans le dictionnaire fourni
 */
void pickRandomWord()
{
	FILE *dico = fopen(filename, "r");
	int taille_dico;
	fscanf(dico, "%d\n", &taille_dico);
	int index_mot = rand() % taille_dico;
	for (int i = 0; i < index_mot; i++) {
		fscanf(dico, "%s\n", motadeviner);
	}
	fclose(dico);
}

/**
 * pendu -
 * @numsockclient: Numéro de la socket du client
 * Description: Programme principal gérant le déroulement d'une partie
 */
void pendu(int numsockclient)
{
	srand(time(NULL));	//Initialise l'aléatoire pour pouvoir utiliser rand()
	char *str_recv = malloc(sizeof(char) * 100);	//Buffer de réception

	/* Initialisation du Mot aleatoire et du nombre de coups initial */
	pickRandomWord();	//Choix du mot
#ifdef DEBUG
	printf("Mot à trouver : %s\n", motadeviner);
#endif
	int wordsize = strlen(motadeviner);
	for (int i = 0; i < wordsize; i++) {	//On créé le mot ne contenant que des tirets, celui qui sera envoyé au client
		mottrouver[i] = '-';
	}
	mottrouver[wordsize] = '\0';
	char nbCoups = 10;	//Initialisation du nombre de coups
	/* ------------------------------------------------------------ */

	do {			/* Echanges tant que la partie n'est pas finie */
		h_writes(numsockclient, mottrouver, 100);	//On envoi le mot à trouver (avec les tirets)
		h_writes(numsockclient, &nbCoups, 1);	//On envoi le nombre de coups
		h_reads(numsockclient, str_recv, 100);	// On récupère la saisie du client
		if (!check(str_recv[0])) {	//On check si le caractère reçu est dans le mot et on met à jour le mot et le nbCoups
			nbCoups--;
		}
	}
	while (strcmp(motadeviner, mottrouver) != 0 && nbCoups != 0);
#ifdef DEBUG
	printf("Fin de partie\n");
	printf((strcmp(motadeviner, mottrouver) ==
		0) ? "Victoire !\n" : "Défaite !\n");
#endif
	h_writes(numsockclient, "jeutermine", 100);	//Dernier envoi, message indiquant la fin de partie
	h_writes(numsockclient, &nbCoups, 1);	//Renvoie du nombre de coup
	if (nbCoups == 0)	//Si il s'agit d'une défaite
	{
		h_writes(numsockclient, motadeviner, 100);	//On envoie le mot qui était à deviner
	}
}

int main(int argc, char *argv[])
{

	char *service = SERVICE_DEFAUT;	/* numero de service par defaut */

	/* Permet de passer un nombre de parametre variable a l'executable */
	switch (argc) {
	case 1:
		printf("defaut service = %s\n", service);
		break;
	case 2:
		service = argv[1];
		break;

	default:
		printf("Usage:serveur service (nom ou port) \n");
		exit(1);
	}

	/* service est le service (ou numero de port) auquel sera affecte
	   ce serveur */

	serveur_appli(service);
}

void serveur_appli(char *service)
/* Procedure correspondant au traitement du serveur de votre application */
{
	int nbConnectionMax = 2;
	int nbConnection = 0;
	int pid = 1;
	int numsockclient;

	/* Initialisation de la socket */
	struct sockaddr_in *p_adr_serv;
	int numsockserveur = h_socket(AF_INET, SOCK_STREAM);	//Création de la socket
	adr_socket(SERVICE_DEFAUT, "127.0.0.1", SOCK_STREAM, &p_adr_serv);	// Configuration de l'adresse de la socket

	h_bind(numsockserveur, p_adr_serv);	//Association de la socket avec son adresse
	h_listen(numsockserveur, nbConnectionMax);	//La socket passe en écoute

	while (nbConnection < nbConnectionMax && pid != 0) {	//On accepte de nouvelles connexion tant qu'on a pas atteint le nombre maximal
		//et tant qu'on est dans le processus père
		numsockclient = h_accept(numsockserveur, p_adr_serv);	//On recupere le numero de la socket client
		nbConnection++;	//On incrémente le nombre de connexion
#ifdef DEBUG
		printf
		    ("Nouvelle connexion, il reste %d connexions disponibles\n",
		     nbConnectionMax - nbConnection);
#endif
		pid = fork();	//On fork le serveur pour paralléliser les exécutions
	}

	if (pid == 0) {		//Si on est dans le fils
#ifdef DEBUG
		printf("Une nouvelle instance de jeu est lancée\n");
#endif
		char play = 1;
		while (play) {	//Tant que l'utilisateur veut jouer
#ifdef DEBUG
			printf("Un nouvelle partie est lancée\n");
#endif
			pendu(numsockclient);	//On démarre une nouvelle partie
			h_reads(numsockclient, &play, 1);	//On regarde si le client souhaite rejouer
		}
#ifdef DEBUG
		printf("Fin définitive\n");
#endif
	} else {		//Si on est dans le père et qu'on a plus de connexions disponibles
#ifdef DEBUG
		printf("Fermeture du serveur\n");
#endif
		h_close(numsockserveur);
	}
}

/******************************************************************************/
