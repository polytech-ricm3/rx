#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

char motadeviner[100];
char mottrouver[100];
char *filename = "dico.dic";
char suggestion;

void check(char c)
{
	for (int i = 0; i < strlen(motadeviner); i++) {
		if (c == motadeviner[i] || c - 32 == motadeviner[i])	//Gère les majuscules
			mottrouver[i] = motadeviner[i];
	}
}

void pickRandomWord()
{
	FILE *dico = fopen(filename, "r");
	int taille_dico;
	fscanf(dico, "%d\n", &taille_dico);
	int index_mot = rand() % taille_dico;
	for (int i = 0; i < index_mot; i++) {
		fscanf(dico, "%s\n", motadeviner);
	}
	fclose(dico);
}

int main(int argc, char const *argv[])
{
    /*** Init Serveur ***/
	srand(time(NULL));
	pickRandomWord();
	//printf("%s\n",motadeviner);
	int size = strlen(motadeviner);
	for (int i = 0; i < size; i++) {
		mottrouver[i] = '-';
	}
	mottrouver[size] = '\0';

    /*** Client ***/
	do {
		printf("%s\n", mottrouver);
		printf("Saisissez votre proposition : ");
		scanf("%c", &suggestion);
		check(suggestion);
		printf("\n");
	} while (strcmp(motadeviner, mottrouver) != 0);
	printf("Victoire ! \n");
	return 0;
}
